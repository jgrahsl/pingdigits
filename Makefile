build:
	docker-compose build

run:
	docker-compose run service

up:
	docker-compose up -d service

bash:
	docker-compose run bash
