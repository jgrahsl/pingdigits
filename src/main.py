import json
import pingparsing
import time
import signal
import sys

def ping():
    ping_parser = pingparsing.PingParsing()
    transmitter = pingparsing.PingTransmitter()
    transmitter.destination = "google.com"
    transmitter.count = 3
    result = transmitter.ping()
    result = ping_parser.parse(result).as_dict()
    return result

def signal_handler(sig, frame):
    sys.exit(0)

def main():
    signal.signal(signal.SIGINT, signal_handler)

    while True:
        metrics = ping()

        if metrics:
            loss_percentage_divided_by_ten = (metrics["packet_loss_rate"] * 100) / 10

            if loss_percentage_divided_by_ten == 0:
                print(f'.',end='', flush=True)
            else:
                print(f'{int(loss_percentage_divided_by_ten)}',end='', flush=True)
        else:
            print(f'x',end='', flush=True)        
        
        time.sleep(10)
        

if __name__ == '__main__':
    main()
