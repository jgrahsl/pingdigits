# pingdigits - a trivial ping console monitor

![License]

Continuously ping a well-known server and print result as single character to the console.

## Motivation

When your home internet connections seems instable, tracking ping results to a well known server on the console seems a good idea.

## Requirements

- Docker
- docker-compose
- make

## Contents

- `Makefile` being the entry point to 
 - `build` for building the docker image
 - `run` for starting a jupyter notebook listening on `127.0.0.1:8888` using password `notebook`
 - `up` for starting the service in background
 - `bash` to drop into a bash running inside the container
- `docker-compose.yaml` configuring the docker image either for running the service or entering a bash
 - Do not use this directly but use the makefile targets instead
- `/src` contains a few examples, see corresponding section below
- `requirements.txt` containing the python dependencies installed during image building

## Operation

Prints `.` for a ping attempt to google.com resulting in zero packet loss.

Prints a digit indicating the percentage of packet loss divided by 10. That means for example, 10% packet loss results in a "1" printed to the console.

## End of Document

```
__________.__              ________  .__       .__  __          
\______   \__| ____    ____\______ \ |__| ____ |__|/  |_  ______
 |     ___/  |/    \  / ___\|    |  \|  |/ ___\|  \   __\/  ___/
 |    |   |  |   |  \/ /_/  >    `   \  / /_/  >  ||  |  \___ \ 
 |____|   |__|___|  /\___  /_______  /__\___  /|__||__| /____  >
                  \//_____/        \/  /_____/               \/ 
```

[License]: https://img.shields.io/badge/license-MIT-brightgreen